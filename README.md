# SGSAopUtil [![license](https://img.shields.io/badge/license-MIT-%23373737)](https://coding.jd.com/Sunglasses/OpenSource_SGSAOPUtil/tree/master)  [![platform](https://img.shields.io/badge/platform-ios%20%7C%20osx%20%7C%20watchos%20%7C%20tvos-blue)](https://coding.jd.com/Sunglasses/OpenSource_SGSAOPUtil/tree/master)  [![platform](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](https://coding.jd.com/Sunglasses/OpenSource_SGSAOPUtil/tree/master)

[京东-平台业务中心](https://gitee.com/jd-platform-opensource)开源的iOS平台轻量级面向切面编程工具。

`SGSAopUtil` 以友好的方式帮助你快速增加切面，并灵活进行`精确切面`、`切面与切面`、`切面与原方法`等的协同编程。

`SGSAopUtil` 源于京东商城移动研发提效平台 [`Sunglasses`](https://sunglasses.jd.com/)，主要用于京东商城App开发团队的研发、测试提效场景，帮助研发团队快速定位、确认问题。不建议用于生产环境。

目前已知在分类等场景下可能会出现问题，欢迎大家踊跃`PR`，共同完善。

## 使用方式

1. 创建切面

> * 新的切面必须放在新的类中，新的类仅需继承`NSObject`
> 
> * 在切面类中重写目标方法即可
>
> * 通过引入 `SGSAopUtil.h`，并调用宏 `sgs_AOP_callOriginalSelector()` 来触发原始方法

如，一下例子为CollectionView的获取Cell的方法前、后增加切面：

```Objective-C
#import "SGSAopUtil.h"
@interface CollectionViewDataSourceAspect : NSObject
@end

@implementation CollectionViewDataSourceAspect
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"【Generate Cell】%@:%zd-%zd",collectionView, indexPath.section, indexPath.item);
    return sgs_AOP_callOriginalSelector();
}
@end
```

2. 添加切面

> * 在合适的时机通过 `+[SGSAopUtil hookWithTargetContainerObject:withNewProcessClass:]` 方法增加切面
> 
> * 需要明确切面的目标：具体的某个对象 / 某个类

如，为所有的CollectionView增加上述切面：

```Objective-C
#import "SGSAopUtil.h"

// ···

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // ···
    Class target = NSClassFromString(@"UICollectionView");
    Class aspect = NSClassFromString(@"CollectionViewDataSourceAspect");
    [SGSAopUtil hookWithTargetContainerObject:target withNewProcessClass:aspect];
    // ···
}

// ···
```

## 安装

直接将项目中的 `SGSAopUtil/` 文件夹下的两个文件拷贝如项目即可

// TODO: 增加Pod/Carthage接入方式

## 使用进阶

### 精确切面

`SGSAopUtil` 允许你进行`类`、`对象`、`方法`的精确切面，每个目标之间相互独立

### 切面协同/切面接力

当一个目标（`类`、`对象`、`方法`）增加了多个切面时，方式执行会以递归的方式将多个切面分别执行，添加切面会将当前切面 `入栈`，执行切面的顺序为 `出栈` 的顺序。

调用 `sgs_AOP_callOriginalSelector()` 会执行目标 `栈` 中的下一个切面，如果已经 `栈底` ，则执行原始方法。

**协同/接力功能**：

- 通过调用 `sgs_AOP_callOriginalSelector()` 之前将当前方法的入参修改，达到影响下一个切面/原方法直接结果的目的。
- 通过接收 `sgs_AOP_callOriginalSelector()` 的返回值，来获取下一个切面/原始方法的执行结果，从而实现结合下一个切面结果的接力。

## 联系我们

使用过程中如有疑问，欢迎联系我们。

**联系方式**：

[![email](https://img.shields.io/badge/email-liuzexiang@jd.com-blue.svg)](mailto:liuzexiang@jd.com)

[![微信](https://img.shields.io/badge/微信-sunoddman-brightgreen.svg)](""))

## License

[MIT license](/LICENSE).