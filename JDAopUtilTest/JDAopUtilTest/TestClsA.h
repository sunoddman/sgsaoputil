//
//  TestClsA.h
//  JDAopUtilTest
//
//  Created by 刘泽祥 on 24/08/2020.
//  Copyright © 2020 JD. Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestClsA : NSObject

@property (nonatomic, assign) NSInteger lastResult;

+ (void)test;
- (void)test;

- (NSInteger)a:(NSInteger)a plusB:(NSInteger)b;

@end

@interface TestClsB : TestClsA

@end

NS_ASSUME_NONNULL_END
