//
//  AopA.m
//  JDAopUtilTest
//
//  Created by 刘泽祥 on 24/08/2020.
//  Copyright © 2020 JD. Inc. All rights reserved.
//

#import "AopA.h"
#import "SGSAopUtil.h"

@implementation AopA

+ (void)test {
    NSLog(@"【AOP-A +test】Before");
    sgs_AOP_callOriginalSelector();
    NSLog(@"【AOP-A +test】After");
}

- (NSInteger)a:(NSInteger)a plusB:(NSInteger)b {
    NSLog(@"【AOP-A a+b】Before");
    NSInteger result = [sgs_AOP_callOriginalSelector() integerValue];
    NSLog(@"【AOP-A a+b】After");
    self.lastResult = result; // cache last result
    
    return result;
}

@end
