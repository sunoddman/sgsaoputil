//
//  AopB.h
//  JDAopUtilTest
//
//  Created by 刘泽祥 on 24/08/2020.
//  Copyright © 2020 JD. Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AopB1 : NSObject

@end


@interface AopB2 : NSObject

@end

NS_ASSUME_NONNULL_END
