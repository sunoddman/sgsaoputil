//
//  AopA.h
//  JDAopUtilTest
//
//  Created by 刘泽祥 on 24/08/2020.
//  Copyright © 2020 JD. Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AopA : NSObject

@property (nonatomic, assign) NSInteger lastResult;

@end

NS_ASSUME_NONNULL_END
