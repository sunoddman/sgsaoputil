//
//  TestClsA.m
//  JDAopUtilTest
//
//  Created by 刘泽祥 on 24/08/2020.
//  Copyright © 2020 JD. Inc. All rights reserved.
//

#import "TestClsA.h"

@implementation TestClsA

+ (void)test {
    NSLog(@"【A +test】");
}

- (void)test {
    NSLog(@"【A -test】%zd", self.lastResult);
}

- (NSInteger)a:(NSInteger)a plusB:(NSInteger)b {
    return a + b;
}

@end

@implementation TestClsB

@end
