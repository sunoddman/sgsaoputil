//
//  AopB.m
//  JDAopUtilTest
//
//  Created by 刘泽祥 on 24/08/2020.
//  Copyright © 2020 JD. Inc. All rights reserved.
//

#import "AopB.h"
#import "SGSAopUtil.h"

@implementation AopB1

- (NSInteger)a:(NSInteger)a plusB:(NSInteger)b {
    NSLog(@"【AOP-B1 a+b】Before");
    
    a = -(a);
    b = -(b);
    NSInteger result = [sgs_AOP_callOriginalSelector() integerValue];
    NSLog(@"【AOP-B1 a+b】After");
    
    return result;
}

@end

@implementation AopB2

static char *k_lastNumA = "k_lastNumA";

- (NSInteger)lastNumA {
    return [objc_getAssociatedObject(self, k_lastNumA) integerValue];
}

- (void)setLastNumA:(NSInteger)a {
    objc_setAssociatedObject(self, k_lastNumA, @(a), OBJC_ASSOCIATION_ASSIGN);
}

static char *k_lastNumB = "k_lastNumB";

- (NSInteger)lastNumB {
    return [objc_getAssociatedObject(self, k_lastNumB) integerValue];
}

- (void)setLastNumB:(NSInteger)b {
    objc_setAssociatedObject(self, k_lastNumB, @(b), OBJC_ASSOCIATION_ASSIGN);
}


- (NSInteger)a:(NSInteger)a plusB:(NSInteger)b {
    NSLog(@"【AOP-B2 a+b】Before");
    NSInteger result = [sgs_AOP_callOriginalSelector() integerValue];
    NSLog(@"【AOP-B2 a+b】After");
    
    [self setLastNumA:a];
    [self setLastNumB:b];
    return -result;
}

- (void)test {
    NSLog(@"【AOP-B2 test】%zd + %zd", [self lastNumA], [self lastNumB]);
    sgs_AOP_callOriginalSelector();
}

@end
