//
//  ViewController.m
//  JDAopUtilTest
//
//  Created by 刘泽祥 on 24/08/2020.
//  Copyright © 2020 JD. Inc. All rights reserved.
//

#import "ViewController.h"
#import "SGSAopUtil.h"
#import "TestClsA.h"


@interface CollectionCell1 : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lab;

@end

@implementation CollectionCell1
@end

@interface ViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView1;

@property (weak, nonatomic) IBOutlet UILabel *labNum1;
@property (weak, nonatomic) IBOutlet UILabel *labNum2;
@property (weak, nonatomic) IBOutlet UILabel *labResult;

@property (strong, nonatomic) TestClsA *caculator_a;
@property (strong, nonatomic) TestClsB *caculator_b1;
@property (strong, nonatomic) TestClsB *caculator_b2;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView1.dataSource = self;
    self.collectionView1.delegate = self;

    self.caculator_a = [TestClsA new];
    self.caculator_b1 = [TestClsB new];
    self.caculator_b2 = [TestClsB new];
    
    // load AOP
    Class clsA = NSClassFromString(@"TestClsA");
    Class aopA = NSClassFromString(@"AopA");
    [SGSAopUtil hookWithTargetContainerObject:clsA withNewProcessClass:aopA];
    
    Class aopB1 = NSClassFromString(@"AopB1");
    [SGSAopUtil hookWithTargetContainerObject:self.caculator_b1 withNewProcessClass:aopB1];
    
    Class aopB2 = NSClassFromString(@"AopB2");
    [SGSAopUtil hookWithTargetContainerObject:self.caculator_b2 withNewProcessClass:aopB2];
    [SGSAopUtil hookWithTargetContainerObject:self.caculator_b2 withNewProcessClass:aopB1];
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CollectionCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuse1" forIndexPath:indexPath];
    cell.lab.text = [NSString stringWithFormat:@"%zd", indexPath.item];
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 10;
}

static NSString *emptyStr = @"______";

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.labNum1.text isEqualToString:emptyStr]) {
        self.labNum1.text = [NSString stringWithFormat:@"%zd", indexPath.item];
    } else if ([self.labNum2.text isEqualToString:emptyStr]) {
        self.labNum2.text = [NSString stringWithFormat:@"%zd", indexPath.item];
    }
}

- (IBAction)calculateA:(id)sender {
    NSInteger result = [self.caculator_a a:[self.labNum1.text integerValue] plusB:[self.labNum2.text integerValue]];
    self.labResult.text = [NSString stringWithFormat:@"%zd", result];
}

- (IBAction)calculateB1:(id)sender {
    NSInteger result = [self.caculator_b1 a:[self.labNum1.text integerValue] plusB:[self.labNum2.text integerValue]];
    self.labResult.text = [NSString stringWithFormat:@"%zd", result];
}

- (IBAction)calculateB2:(id)sender {
    NSInteger result = [self.caculator_b2 a:[self.labNum1.text integerValue] plusB:[self.labNum2.text integerValue]];
    self.labResult.text = [NSString stringWithFormat:@"%zd", result];
}

- (IBAction)clear:(id)sender {
    self.labNum1.text = emptyStr;
    self.labNum2.text = emptyStr;
    self.labResult.text = emptyStr;
}

- (IBAction)callB2Test:(id)sender {
    [self.caculator_b2 test];
}


@end
